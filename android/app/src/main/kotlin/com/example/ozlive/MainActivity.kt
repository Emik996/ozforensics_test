package com.ozforensics.liveness.sample

import androidx.annotation.NonNull
import com.example.ozlive.ForensicsSdkBridge
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        ForensicsSdkBridge.registerForensics(flutterEngine, this)

    }

}
