package com.example.ozlive

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import androidx.core.app.ActivityCompat.startActivity
import androidx.core.app.ActivityCompat.startActivityForResult
import com.ozforensics.liveness.sample.R
import com.ozforensics.liveness.sdk.core.OzLivenessSDK
import com.ozforensics.liveness.sdk.core.StatusListener
import com.ozforensics.liveness.sdk.core.exceptions.OzException
import com.ozforensics.liveness.sdk.core.model.OzAction
import com.ozforensics.liveness.sdk.customization.*
import com.ozforensics.liveness.sdk.security.LicenseSource
import com.ozforensics.liveness.sdk.security.model.LicensePayload
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel


object ForensicsSdkBridge {
    private const val FORENSICS_SDK_CHANNEL = "kz.ndfs.app/forensics"

    fun registerForensics(mFlutterEngine: FlutterEngine, context: Context) {

        MethodChannel(mFlutterEngine.dartExecutor.binaryMessenger, FORENSICS_SDK_CHANNEL)
            .setMethodCallHandler { methodCall, result1 ->
                when (methodCall.method) {
                    "startForensics" -> {
                        OzLivenessSDK.init(listOf(LicenseSource.LicenseAssetId(R.raw.forensics)),
                            object : StatusListener<LicensePayload> {
                                override fun onSuccess(result: LicensePayload) {
//                                   result1.success("QWEQWEQWEQWEQWQWEQWEQWEQWEQWEQWE")
                                }
                                override fun onError(error: OzException) {
                                    val methodArgument: String = methodCall.argument("token")!!
//                                    result1.error(methodArgument, error.toString(), null)
                                }
                            }
                        )
                    OzLivenessSDK.config.customization = OzCustomization(
                    OzCancelButtonCustomization(R.drawable.ic_close),
                    OzCenterHintCustomization(16f, Typeface.MONOSPACE, Color.CYAN, 1.3f),
                    OzDialogCustomization(R.style.Sdk_Dialog_Theme),
                    OzFaceFrameCustomization(OzFaceFrameCustomization.GeometryType.OVAL, 8f, 5f, Color.RED, Color.GREEN),
                    OzVersionTextCustomization(Color.WHITE))

                        val intent = OzLivenessSDK.createStartIntent(listOf(OzAction.Smile, OzAction.Scan))
                        startActivity(context, intent, null)


                    }



                    else -> result1.notImplemented()
                }
            }
    }

}